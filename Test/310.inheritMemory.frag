#version 310 es
precision mediump float;

struct S {
    float buff[10];
};

layout(std140, binding=2) readonly buffer RoBuff {
    S s_ro;
} ro_buffer;

layout(std430, binding=2) buffer Buff {
    S s;
} non_ro_buffer;

void non_ro_funS(S s) { }

out vec4 fragColor;

void main()
{
    S s;

    non_ro_funS(ro_buffer.s_ro);
    non_ro_funS(non_ro_buffer.s);
    non_ro_funS(s);
}
